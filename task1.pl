#!/usr/bin/perl

use strict;
use File::Find;
use File::Basename;

#read in the run parameters as the directories that will be searched
my @searchDirectories = @ARGV;

my @renameCounts = (0, 0, 0);

unless(@searchDirectories)
{
   die "Error: Please specify a directory in the run parameters.\n";
}

#map the hexidecimal image codes to their human-readable counter parts
my %fileTypes = (
	"474946" => "gif",
	"504e47" => "png",
	"ffd8ff" => "jpg",
);

foreach ( @searchDirectories )
{
	print ("\nSearching directory: $_\n");

	if (-e "$_") {
		#search through the directory specified and sub-directories for any files
		find(\&found_files, "$_");

		#output the number of different image extensions that were renamed
		print ("\nNumber of GIF files renamed: $renameCounts[0]\n");
		print ("Number of PNG files renamed: $renameCounts[1]\n");
		print ("Number of JPG files renamed: $renameCounts[2]\n\n");

		@renameCounts = (0, 0, 0);
	}
	else
	{
		print ("Error: Directory '$_' cannot be found\n");
	}
}

sub found_files
{
	print "\nFile: $_ -> ";

	#prevent the verification and renaming process to be performed on folders
	if((!-d) && ("$_" ne "."))
	{
		#open the file in readonly mode, storing the first line before closing it
		open(my $fh, "<$_") or die "Error: File could not be opened. $!\n";

		my $firstLine = (<$fh>);

		close($fh);

		#read the raw image data as hexidecimal
		my $hex = unpack("H*", $firstLine);

		for my $type (keys %fileTypes)
		{
			if ($hex =~ $type)
			{ 
			    #perform a regex operation to find the file extension
			    my $fileExtension = (fileparse("$_", qr/\.[^.]*/))[2];

				my $fileName = "$_";

				#only remove the current file extension if it actually exists
				if (length($fileExtension) > 0)
				{
					$fileName = substr("$_", 0, -length($fileExtension));
				}

				my $newFileName = "$fileName.$fileTypes{$type}";

				#only rename files which have different file names to what they should have
				if ("$_" ne $newFileName)
				{
					print ("File detected as a $fileTypes{$type}, new file name will be -> $newFileName\n");

					rename("$_", $newFileName) or die ("Error: Could not rename file. $!\n");

					#save a count of the different file extensions which were modified
					if ("$fileTypes{$type}" eq "gif")
					{
						$renameCounts[0]++;
					}
					elsif ("$fileTypes{$type}" eq "png")
					{
						$renameCounts[1]++;
					}
					elsif ("$fileTypes{$type}" eq "jpg")
					{
						$renameCounts[2]++;
					}
				}
				else
				{
					print ("File already has the correct extension, skipping... \n");
				}

			    return;
			}
		}

		print ("Not an image, skipping... \n");
	}
	else
	{
		print ("Unsupported file format or is folder, skipping... \n");
	}
}